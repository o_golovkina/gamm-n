﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Gamm
{
    class Program
    {
        static Dictionary<int, char> Alphabet = new Dictionary<int, char>();

        static void Main(string[] args)
        {
            Console.WriteLine("  Модуль гаммирования:");
            Console.Write("  N = ");

            int n = 0;
            string str = Console.ReadLine();
            str = str.Trim();

            n = Int32.Parse(str);

            Console.WriteLine("  Алфавит для шифрования:");

            for(int i = 0; i < n; i++)
            {
                Console.Write($"  [{i}] = ");
                Alphabet.Add(i, Console.ReadLine().ToUpper().First());
            }

            Console.WriteLine("\n  Полученный алфавит:");

            for (int i = 0; i < n; i++)
                Console.Write(" {0,2} ", i);


            Console.WriteLine();

            foreach (var a in Alphabet)
                Console.Write(" {0,2} ", a.Value);

            Console.WriteLine("\n  Введите ключ для шифрования:");
            Console.Write("  ");
            string key = Console.ReadLine().ToUpper();
            Console.WriteLine("\n  Введите сообщение, которое надо зашифровать:");
            Console.Write("  ");
            string message = Console.ReadLine().ToUpper();

            string res = GammaNEncrypt(key, message);
            Console.WriteLine($"  Результат шифрования:\n  {res}");
            
            res = GammaNDecrypt(key, res);
            Console.WriteLine($"  Результат расшифрования:\n  {res}");
            Console.ReadKey();
        }

        static string GammaNEncrypt(string key, string message)
        {
            key = CorrectKey(key, message);
            string resMessage = string.Empty;

            for (int i = 0; i < message.Length; i++)
            {
                int k = 0, x = 0, z = 0;

                foreach (var a in Alphabet)
                {
                    if (key[i] == a.Value) k = a.Key;
                    if (message[i] == a.Value) x = a.Key;
                    z = (x + k) % Alphabet.Count;
                }
                resMessage += Alphabet[z];
            }
            return resMessage;
        }

        static string GammaNDecrypt(string key, string message)
        {
            key = CorrectKey(key, message);
            string resMessage = string.Empty;

            for (int i = 0; i < message.Length; i++)
            {
                int k = 0, x = 0, z = 0;

                foreach (var a in Alphabet)
                {
                    if (key[i] == a.Value) k = a.Key;
                    if (message[i] == a.Value) x = a.Key;
                    z = (x + Alphabet.Count - k) % Alphabet.Count;
                }
                resMessage += Alphabet[z];
            }
            return resMessage;
        }

        private static string CorrectKey(string key, string message)
        {
            int count = message.Length - key.Length;

            if (count > 0)
            {
                for (int i = 0; i < count; i++)
                    key += key[i];
            }
            else if (count < 0)
            {
                key = key.Remove(message.Length, Math.Abs(count));
            }

            return key;
        }

    }
}
